package ch.bbw;

import java.sql.SQLOutput;

public class Stein {
    Integer left;
    Integer right;

    public Stein(Integer left, Integer right) {
        this.left = left;
        this.right = right;
    }

    public Integer getLeft() {
        return left;
    }

    public void setLeft(Integer left) {
        this.left = left;
    }

    public Integer getRight() {
        return right;
    }

    public void setRight(Integer right) {
        this.right = right;
    }

    public void print() {
        System.out.println("Left: " + this.getLeft() + ", Right: " + this.getRight());
    }

    public Stein flip() {
        return new Stein(right, left);
    }
}
