package ch.bbw;

import java.io.*;
import java.util.*;

public class Game {


    public void start() {
        try {
            List<Stein> dominoList = new ArrayList<>();
            System.out.println("Reading Domino Steine");
            String list = "src/main/resources/dominoSteine.txt";
            FileReader fileReader = new FileReader(list);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            bufferedReader.lines().forEach((val) -> {
                String[] parts = val.split(" ");
                Stein stein = new Stein(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
                dominoList.add(stein);
            });
            System.out.println("End reading list");
            startGame(dominoList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startGame(List<Stein> dominoList) {
        List<List<Stein>> chains = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            // erster Stein wegnehmen und speichern
            Stein stein = dominoList.remove(0);
            // Stein wieder hinten in die Liste setzen
            dominoList.add(stein);
            // kopie von dominoList erstellen
            List<Stein> freshDominoList = new ArrayList<>(dominoList);
            List<Stein> chain = new ArrayList<>();
            Domino.sort(freshDominoList, chain);
            chains.add(chain);
            System.out.println("Reihe: " + i);
            chain.forEach(Stein::print);
            System.out.println("Länge: " + chain.size());
        }
        List<Stein> longestChain = chains.stream().max(Comparator.comparing(List::size)).get();
        writeLongestToFile(longestChain);
    }

    public void writeLongestToFile(List<Stein> longestChain) {
        try {
            String newFile = "src/main/resources/longestChain.txt";
            PrintWriter pw = new PrintWriter(newFile);
            pw.println("Longest Domino Chain");
            longestChain.forEach(stein -> pw.print(" |" + stein.left + " " + stein.right + "|"));
            pw.println();
            pw.println("Length: " + longestChain.size());
            pw.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
