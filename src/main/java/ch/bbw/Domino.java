package ch.bbw;

import java.util.List;

public class Domino {

    public static void sort(List<Stein> dominoList, List<Stein> reihe) {
        int currentRight;
        for (int i = 0; i < dominoList.size(); i++) {
            if (reihe.isEmpty()) {
                reihe.add(dominoList.get(i));
                dominoList.remove(dominoList.get(i));
                sort(dominoList, reihe);
            }
            int index = reihe.size();
            Stein currentStein = reihe.get(index - 1);
            currentRight = currentStein.right;
            Stein compareStein = dominoList.get(i);
            if (compareStein.left.equals(currentRight)) {
                reihe.add(compareStein);
                dominoList.remove(compareStein);
                sort(dominoList, reihe);
            } else if (compareStein.right.equals(currentRight)) {
                Stein flippedStein = compareStein.flip();
                reihe.add(flippedStein);
                dominoList.remove(compareStein);
                sort(dominoList, reihe);
            }
        }
    }
}
